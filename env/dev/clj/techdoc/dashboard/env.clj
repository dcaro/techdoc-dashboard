(ns techdoc.dashboard.env
  (:require
    [clojure.tools.logging :as log]
    [techdoc.dashboard.dev-middleware :refer [wrap-dev]]))

(def defaults
  {:init       (fn []
                 (log/info "\n-=[Dashboard starting using the development or test profile]=-"))
   :start      (fn []
                 (log/info "\n-=[Dashboard started successfully using the development or test profile]=-"))
   :stop       (fn []
                 (log/info "\n-=[Dashboard has shut down successfully]=-"))
   :middleware wrap-dev
   :opts       {:profile       :dev
                :persist-data? true}})
