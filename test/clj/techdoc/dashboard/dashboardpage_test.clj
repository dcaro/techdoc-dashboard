(ns techdoc.dashboard.dashboardpage-test
  (:require [clojure.test :refer :all]
            [techdoc.dashboard.web.ui.dashboard :as dashboard]))

(deftest table
  (is (= :table.table-fixed.border-collapse.border.border-slate-400.w-full
         (first (flatten (dashboard/table-for-pageviews {:2023052500 1} "en")))))
  (is (= "20230525"
         (some #{"20230525"} (flatten (dashboard/table-for-pageviews {:2023052500 1} "en"))))))

(deftest widget
  (is (= 21
         (count (flatten (dashboard/pageviews-widget "" nil false "en")))))
  (is (= 29
         (count (flatten (dashboard/pageviews-widget "" {:2023082400 1} false "en")))))
  (is (= 33
         (count (flatten (dashboard/pageviews-widget "" {:2023082400 1} true "en")))))
  (is (= :div.chart-box
         (some #{:div.chart-box} (flatten (dashboard/pageviews-widget "" {:2023082400 1} true "en")))))
  (is (= nil
         (some #{:div.chart-box} (flatten (dashboard/pageviews-widget "" {:2023082400 1} false "en"))))))
