(ns techdoc.dashboard.home-test
  (:require [clojure.test :refer :all]
            [techdoc.dashboard.web.ui.home :as home]
            [clojure.string :as str]
            [hiccup.page :as p]))

(deftest button
  (is (= "test"
         (last (home/button {:label "test"}))))
  (is (= :button
         (first (home/button {}))))
  (is (= "Submit"
         (last (home/button {})))))

(deftest home-page-ui 
 (is (= true
        (str/includes? (p/html5 (home/home-page-ui :en "test")) "test")
        (str/includes? (p/html5 (home/home-page-ui :en "test")) "Specify a PagePile ID")
        (str/includes? (p/html5 (home/home-page-ui :pl "test")) "Podaj identyfikator"))))

(deftest home-page
 (is (= true
        (str/includes? (home/home-page {:params {:lang "en"}}) "Specify"))))

(deftest home-page-no-js
 (is (= true
        (str/includes? (home/home-page-no-js {:params {:lang "en" :pagepile 47924}}) "Release_Engineering"))))

(deftest load-pile
 (is (= true
        (str/includes? (home/load-pile {:params {:lang "en" :pagepile 47924}}) "Release_Engineering"))))