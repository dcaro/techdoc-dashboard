(ns techdoc.dashboard.pagepile-test
  (:require [clojure.test :refer :all]
            [techdoc.dashboard.web.controllers.pagepile :as pagepile]
            [techdoc.dashboard.web.controllers.cache :as cache]))

(def test-data
  (read-string (slurp "test/clj/techdoc/dashboard/test_data.edn"))) ;; load PagePile error responses from test-data.edn

(def correct-pagepile
  (pagepile/load-pagepile-through-cache 47924)) ;; load correct PagePile directly from API and cache it

(def cached-correct-pagepile
  (cache/just-fetch-record (keyword "pagepile" (str 47924)))) ;; load cached record

(def incorrect-pagepile
  (:incorrect test-data))

(def forbidden-pagepile
  (:forbidden test-data))

(def connection-error
  (:not-connected test-data))

(deftest validations
  (is (= :ok
         (:status (pagepile/validate-pagepile correct-pagepile :en))))
  (is (= "wikitechwikitechwikimedia"
         (:wiki (pagepile/validate-pagepile correct-pagepile :en))))
  (is (= "Invalid PagePile ID"
         (:body (pagepile/validate-pagepile incorrect-pagepile :en))))
  (is (= "Cannot connect to PagePile"
         (:body (pagepile/validate-pagepile connection-error :en))))
  (is (= "PagePile contains pages from an unsupported wiki"
         (:body (pagepile/validate-pagepile forbidden-pagepile :en)))))

(deftest cache
  (is (= :ok
         (:status (pagepile/validate-pagepile cached-correct-pagepile :en)))))

(deftest getters
  (is (= "Invalid PagePile ID"
         (last (flatten (pagepile/get-pagepile 4999 :en)))))
  (is  (= :ul.leading-8
          (first (flatten (pagepile/get-pagepile 47901 :en)))))
  (is (= true
         (:error (pagepile/get-pagepile-structured 4999 :en))))
  (is (= false
         (:error (pagepile/get-pagepile-structured 47901 :en)))))
