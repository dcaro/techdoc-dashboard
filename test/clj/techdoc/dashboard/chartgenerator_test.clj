(ns techdoc.dashboard.chartgenerator-test
  (:require [clojure.test :refer :all]
            [techdoc.dashboard.lib.chart-generator :as cg]))

(def chart
  (cg/generate-bar-chart (into [] (range 1 91)) "90 values" "Value" "Date"))

(def chart-svg
  (cg/chart-to-svg chart))

(deftest generate-bar-chart
  (is (= :dali/page
         (first (flatten chart))))
  (is (= 224
         (last (flatten chart)))))

(deftest chart-to-svg
  (is (= "<svg xmlns=\"http://www.w3.org/2000/svg\""
         (re-find #"<svg xmlns=\"http://www.w3.org/2000/svg\"" chart-svg)))
  (is (= "</svg>"
         (re-find #"</svg>" chart-svg))))
