(ns techdoc.dashboard.mwclient-test
  (:require [clojure.test :refer :all]
            [techdoc.dashboard.lib.mw-client :as mw]
            [java-time.api :as jt]))

(deftest dates-structure
 (is (= 90 ;; dates structure should contain 90 days
        (count (mw/dates-structure)))
     (= nil;; it should not contain a record for today
        (get (mw/dates-structure) (keyword (str (mw/UTC-date-now) "00")))))
 (is (= 0  ;; a record for yesterday should be 0
        (get (mw/dates-structure) (keyword (jt/format "yyyyMMdd00" (jt/minus (jt/with-zone (jt/zoned-date-time) "UTC") (jt/days 1))))))))

(deftest calculate-sum
  (is (= 2
         (:one (mw/calculate-sum [{:one 1}{:one 1}]))))
  (is (= 0
         (count (mw/calculate-sum [])))))

(deftest pageviews-for-collection
  (is (= 90
         (count (mw/get-pageviews-for-collection ["Help:Toolforge"] "wikitechwikitechwikimedia"))))
  (is (<= 0
         (get (mw/get-pageviews-for-collection ["Help:Toolforge"] "wikitechwikitechwikimedia") (keyword (str (mw/UTC-date-90) "00"))))))
