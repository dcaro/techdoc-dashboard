(ns techdoc.dashboard.locale-test
  (:require [clojure.test :refer :all]
            [techdoc.dashboard.web.controllers.locale :as locale]))

(deftest translations
  (is (= "Load"
         (:button-load (locale/load-translation-file "locale/en.json"))
         (locale/translate :en :button-load)
         (locale/tr "en" :button-load))))
