(ns build
  (:require [clojure.string :as string]
            [clojure.tools.build.api :as b]
            [clojure.java.shell :as shell]))

(def lib 'techdoc/dashboard)
(def main-cls (string/join "." (filter some? [(namespace lib) (name lib) "core"])))
(def version (format "0.0.1-SNAPSHOT"))
(def target-dir "target")
(def class-dir (str target-dir "/" "classes"))
(def uber-file (format "%s/%s-standalone.jar" target-dir (name lib)))
(def jar-file (format "%s/%s.jar" target-dir (name lib)))
(def basis (b/create-basis {:project "deps.edn"}))

(defn build-css
  []
  (apply shell/sh
         (concat ["npx" "tailwindcss"
                  "-c" "resources/tailwind.config.js"
                  "-i" "resources/tailwind.css"
                  "-o" "resources/public/css/app.css"
                  "--minify"])))

(defn clean
  "Delete the build target directory"
  [_]
  (println (str "Cleaning " target-dir))
  (b/delete {:path target-dir}))

(defn prep [_]
  ;;Temporary - build CSS assets using npx
  (println "Building CSS using npx tailwindcss (make sure it's available)!")
  (build-css)
  (println "Writing Pom...")
  (b/write-pom {:class-dir class-dir
                :lib lib
                :version version
                :basis basis
                :src-dirs ["src/clj"]})
  (b/copy-file {:src "LICENSE" :target (str class-dir "/META-INF/LICENSE-TECHDOC-DASHBOARD")})
  (b/copy-file {:src "DEPENDENCIES.txt" :target (str class-dir "/META-INF/DEPENDENCY-LICENSES-TECHDOC-DASHBOARD")})
  (b/copy-dir {:src-dirs ["src/clj" "resources" "env/prod/resources" "env/prod/clj"]
               :target-dir class-dir}))

(defn build-uberjar [_]
  (println "Compiling Clojure...")
  (b/compile-clj {:basis basis
                  :src-dirs ["src/clj" "resources" "env/prod/resources" "env/prod/clj"]
                  :class-dir class-dir})
  (println "Making uberjar...")
  (b/uber {:class-dir class-dir
           :uber-file uber-file
           :main main-cls
           :basis basis}))

(defn build-jar [_]
  (println "Compiling Clojure...")
  (b/compile-clj {:basis basis
                  :src-dirs ["src/clj" "resources" "env/prod/resources" "env/prod/clj"]
                  :class-dir class-dir})
  (println "Making jar...")
  (b/jar {:class-dir class-dir
          :main main-cls
          :basis basis
          :jar-file jar-file}))

(defn uberjar [_]
  (do (clean nil) (prep nil) (build-uberjar nil)))

(defn jar [_]
  (do (clean nil) (prep nil) (build-jar nil)))
