# Decision records

This file documents decisions made in the technical documentation dashboard project.

## Programming language, web framework, and front-end technologies

### Status

* Accepted on 2023-05-12
* Proposed on 2023-02-27

### Context

Choice of a technology stack for an application is incredibly important. It determines the ease of development, availability of libraries, as well as likelihood of attracting contributors.

Main considerations taken into account for this proposal:

* ease of development and maintenance for a single developer
* ease of finding contributors
* ease of deployment on Toolforge
* difficulty (approachability) and complexity of the technologies themselves
* accessibility and internationalization feature support
* client-side device requirements (CPU/memory/internet connection quality)
* availability of libraries

### Decision

Use Clojure as the main programming language. Use the Kit framework as the starting point of the web application functionality. Use HTMX and hyperscript to progressively enhance the dashboard as a multi-page application. Use Tailwind CSS for styling.

Don't use Vue or React and reduce third-party dependencies outside the Clojure ecosystem during development and in production.

#### Decision-making process

A decision matrix for different technologies, with fields populated based on prototypes implemented in these technologies.

#### Rationale - Clojure

Clojure is a mature, functional programming language. It runs on the Java Virtual Machine, making it easy to deploy. It's a Lisp, which makes the code more concise and easy to reason about (once you get used to the parentheses).

Clojure has a low rate of code churn/high rate of code retention. This makes core APIs stable. This practice is also common in third-party libraries, so there is rarely a need to change code merely due to library or language version upgrades.

Clojure has a rich ecosystem of third-party libraries. These libraries are often minimalistic in their design, aiming to fulfill specific, individual goals. They don't grow infinitely - only to a point when they're considered complete and stable. This makes Clojure libraries reliable and straightforward to use.

One downside of Clojure is that it's not popular among members of the Movement. That said, introducing a new project in Clojure can become a starting point for its rise in popularity.

Other languages considered:

* Python
* JavaScript/TypeScript/ClojureScript
* Elixir

#### Rationale - Kit

Kit uses Integrant, which implements data-driven design principles. It's a modern, reliable way of building software.

Kit is developed by the creators of Luminus, the most popular web framework in Clojure, and builds on their earlier experience.

Kit is modular, with little code of its own, which makes it easy to build around.

Other frameworks considered:

* Biff
* Luminus
* Django
* Flask

#### Rationale - HTMX

HTMX is a small, modern front-end library. It will allow the project to meet the front-end payload size requirements while providing dynamic functionality where necessary.

Other frameworks or libraries considered:

* React
* Vue
* Re-frame/Reagent

#### Rationale - Tailwind CSS

Tailwind CSS is a modern way of managing CSS code. It doesn't enforce any particular style or require additional dependencies or tools, but provides reasonable defaults.

It has support for building CSS components without introducing unnecessary syntax. It integrates with hiccup and Clojure well.

Other frameworks or libraries considered:

* Bootstrap
* Codex

### Consequences

The selected technology stack should make it possible to build an application that fulfills the requirements while remaining intuitive and maintainable.

Technologies such as Clojure and Kit might be an obstacle for potential contributors. These technologies use less popular programming paradigms and practices (functional programming, data-driven architecture). That said, this presents an opportunity to show viability of these technologies and paradigms within the wiki ecosystem.
