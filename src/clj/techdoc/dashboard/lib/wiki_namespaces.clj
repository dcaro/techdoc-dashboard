(ns techdoc.dashboard.lib.wiki-namespaces
  "Gives you access to namespaces used on permitted-wikis.
  Loads the namespaces when the application is started."
  (:require [clj-http.client :as client]
            [techdoc.dashboard.config :as config]))

(defn fetch-namespaces
  "Requests and directly returns a map containing namespaces
  available on the specified `wiki`. Use `format-namespaces-response`
  to prepare this data for usage in SQL query generation."
  [wiki]
  (client/get (get-in config/permitted-wikis [wiki :api])
              {:headers {"User-Agent" config/user-agent}
               :as :json
               :query-params {"action" "query"
                              "meta" "siteinfo"
                              "siprop" "namespaces"
                              "format" "json"
                              "formatversion" 2}}))

(defn format-namespaces-response
  "Reformats the response returned from querying wiki APIs about available
  namespaces into a structure more easily applied for generating SQL queries.

  Example of the returned data structure:
  {\"Manual\" 100
   nil 0}

  Takes the unedited response `r` as the only parameter"
  [r]
  (let [namespaces (get-in r [:body :query :namespaces])]
    (reduce-kv #(assoc %1 (:canonical %3) (read-string (subs (str %2) 1))) {} namespaces)))

(defn get-wiki-namespaces
  [wiki]
  (-> wiki
      fetch-namespaces
      format-namespaces-response))

;;TODO: Move this to app startup
;;Otherwise it gets called during production build
(def wiki-namespaces
  {"mediawikiwiki" (get-wiki-namespaces "mediawikiwiki")
   "metawiki" (get-wiki-namespaces "metawiki")
   "wikitechwikitechwikimedia" (get-wiki-namespaces "wikitechwikitechwikimedia")})
