(ns techdoc.dashboard.lib.replica-client
  "Functions for interacting with wiki replicas."
  (:require [honey.sql :as sql]
            [clojure.string :as str]
            [techdoc.dashboard.lib.wiki-namespaces :as wn]))

(sql/set-dialect! :mysql)

; only for test purposes
(def list-of-pages
  ["Portal:Cloud_VPS"
   "Help:Toolforge/Pywikibot"
   "Help:Toolforge"
   "Wiki_Replicas"])

(defn process-page
  [s p w]
  (let [r (str/split p #":" 2)
        l (count r)]
    (if (and (= l 2) (some? (get-in wn/wiki-namespaces [w (nth r 0)])))
      (conj s [:and
               [:= :p.page_title (nth r 1)]
               [:= :p.page_namespace (get-in wn/wiki-namespaces [w (nth r 0)])]])
      s)))

(defn gen-where
  [l w]
  (reduce (fn [s p] (process-page s p w)) [:or] l))


(defn generate-constraints
  [l w]
  {:in l
   :where (gen-where l w)})

(defn gen-revisions-sql
  [l w]
  (let [constraints (generate-constraints l w)]
    (sql/format {:select [:*],
                 :from [[:page :p]]
                 :join-by [:left [[:revision :r]
                                  [:= :r.rev_page :p.page_id]]
                           :left [[:actor :a]
                                  [:= :r.rev_actor :a.actor_id]]]
                 :where [:and
                         [:= :r.rev_deleted 0]
                         [:or
                          [:in :p.page_title (:in constraints)]
                          (:where constraints)]]
                 :order-by [:r.rev_timestamp]}
                {:pretty true})))

;SQL query we want to generate
;;select * from page p
;;left join (revision r, actor a)
;;on (r.rev_page=p.page_id and r.rev_actor=a.actor_id)
;;where r.rev_deleted = 0
;;and (p.page_title IN  ('Portal:Cloud_VPS', 'Help:Toolforge/Pywikibot')
;;     or (p.page_title = 'Toolforge/Pywikibot' and p.page_namespace = 12))
;;order by r.rev_timestamp;
