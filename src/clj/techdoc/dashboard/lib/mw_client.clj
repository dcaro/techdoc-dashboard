(ns techdoc.dashboard.lib.mw-client
  "Functions for interacting with MediaWiki APIs."
  (:require [clj-http.client :as client]
            [throttler.core :refer  [fn-throttler]]
            [techdoc.dashboard.config :as config]
            [java-time.api :as jt]
            [techdoc.dashboard.web.controllers.cache :refer [fetch-cache-record]]
            [clojure.string :as str]
            [ring.util.codec :refer [url-encode]]
            [clojure.tools.logging :as log]))

(def
  ^{:doc "Creates a rate-limiting configuration for Action API requests: 1/s.
          Use it when defining a rate-limited version of a fetch function."}
  api-throttler (fn-throttler 1 :second))
(def
  ^{:doc "Creates a rate-limiting configuration for REST API requests: 90/s.
          Use it when defining a rate-limited version of a fetch function."}
  rest-throttler (fn-throttler 90 :second))

(defn fetch-general-page-data
  "Fetches information about outgoing and incoming links,
  general page info, categories, and templates used on a `page`
  on a given `wiki`. For a list of supported wikis, see [[config/permitted-wikis]]

  Example:
  ```(fetch-general-page-data \"Help:Toolforge\" \"wikitechwikitechwikimedia\")```"
  [page wiki]
  (let [wiki-url (get-in config/permitted-wikis [wiki :api])]
    (client/get wiki-url
                {:headers {"User-Agent" config/user-agent}
                 :as :json
                 :query-params {"action" "query"
                                "format" "json"
                                "prop" "linkshere|links|info|categories|templates"
                                "titles" page
                                "lhlimit" "max"
                                "cllimit" "max"
                                "pllimit" "max"}})))

(defn fetch-page-preview
  "Fetches content of a `page` from a given `wiki`.
  For a list of supported wikis, see [[config/permitted-wikis.]]

  Example:
  ```(fetch-page-preview \"Help:Toolforge\" \"wikitechwikitechwikimedia\")```"
  [page wiki]
  (client/get (get-in config/permitted-wikis [wiki :api])
              {:headers {"User-Agent" config/user-agent}
               :as :json
               :query-params {"action" "parse"
                              "format" "json"
                              "page" page}}))

(defn dates-structure
  "Generates a map with a zero for every day in the last 90 days. Intended to be
  used with [[calculate-sum]]. This ensures that the pageviews data structure
  always has records for all 90 days. It is necessary because the API does not
  return data for days with zero pageviews, making it difficult to draw the chart correctly.
  "
  []
  (let [now (jt/minus(jt/with-zone (jt/zoned-date-time) "UTC") (jt/days 1))]
    (->> (take 90 (jt/iterate jt/minus now (jt/days 1)))
         (reduce #(conj %1 {(keyword (jt/format "yyyyMMdd00" %2)) 0}) {}))))

(defn UTC-date-now
  "Returns current UTC date"
  []
  (jt/format "yyyyMMdd" (jt/with-zone (jt/zoned-date-time) "UTC")))

(defn UTC-date-90
  "Returns UTC date of the day 90 days ago"
  []
  (jt/format "yyyyMMdd" (jt/minus (jt/with-zone (jt/zoned-date-time) "UTC") (jt/days 90))))

(defn- fetch-page-views-data
  "Fetches pageview data based on the required parameters. Only use this function
  for testing and troubleshooting purposes. In other circumstances, use the
  rate-limited and cached functions listed below."
  [page wiki d-90 d-now]
  (log/info "Fetching page view data for: " page)
  (->
   (try (client/get (str "https://wikimedia.org/api/rest_v1/metrics/pageviews/per-article/"
                         (get-in config/permitted-wikis [wiki :id])
                         "/all-access/all-agents/"
                         (url-encode page)
                         "/daily/"
                         d-90
                         "/"
                         d-now)
                    {:as :json
                     :headers {"User-Agent" config/user-agent}
                     :throw-entire-message? true})
        (catch Exception e
          (log/warn "Error: " (.getMessage e))
          {:body {:items []}}))
   (get-in [:body :items])))

;Rate-limited versions of fetch functions implemented above

(def ^{:doc "Rate-limited version of [[fetch-page-preview]]"}
  fgp-slow (api-throttler fetch-page-preview))
(def ^{:doc "Rate-limited version of [[fetch-general-page-data]]"}
  fgpd-slow (api-throttler fetch-general-page-data))
(def ^{:doc "Rate-limited version of [[fetch-page-views-data]]"}
  fpvd-slow (rest-throttler fetch-page-views-data))

(defn- fetch-page-views-data-basedon-key
  "Fetches pageview data based on a cache key. The key must have the following format:
  `:pageviews/<wiki>~~|<page>~~|<date from yyyyMMdd>~~|<date to yyyyMMdd>`, for example
  `:pageviews/wikitechwikitechwikimedia~~|Help:Toolforge~~|20230407~~|20230706`.

  For dates, use the UTC-date-now and UTC-date-90 functions. For a list of permitted wikis,
  see [[config/permitted-wikis]]. Page title is automatically escaped inside the function.

  Only use this function for troubleshooting. In normal circumstances, use
  [[fetch-pageviews-through-cache]]."
  [k]
  (let [id (str/split (name k) #"~~\|")
        wiki (nth id 0)
        page (nth id 1)
        date-90 (nth id 2)
        date-now (nth id 3)]
    (fpvd-slow page wiki date-90 date-now)))

(defn fetch-pageviews-through-cache
  "Returns pageview information for a given `page` on a given `wiki`. Uses cache first.
  If the data is not available in cache, it issues a call to the API, then stores the response
  in the cache. For a list of supported wikis, see [[config/permitted-wikis]].

  This function uses a rate-limited fetch function and might take a while to run."
  [page wiki]
  (fetch-cache-record (keyword "pageviews" (str wiki "~~|" page "~~|" (UTC-date-90) "~~|" (UTC-date-now))) #(fetch-page-views-data-basedon-key %)))

(defn extract-pageviews-from-response
  "Reformats the pageview response (from cache or API) by removing unnecessary information.
  Makes it easier to process the data further. Takes response `r` returned by the
  [[fetch-pageviews-through-cache]] function as argument."
  [r]
  (reduce (fn [m v] (assoc m (keyword (:timestamp v)) (:views v))) {} r))

(defn get-pageviews-for-page
  "Returns reformatted pageview information for a `page` on a given `wiki`.
  For a list of supported wikis, see [[config/permitted-wikis]]."
  [page wiki]
  (-> page
      (fetch-pageviews-through-cache wiki)
      (extract-pageviews-from-response)))

(defn calculate-sum
  "Merges a collection of maps (for example, containing pageview values for different pages)
  into a single map, adding values under the same key.

  Addition works like below:

  `[{:a 10 :b 20} {:a 1 :b 2}]` becomes:
  `{:a 11 :b 22}`
  "
  [collection]
  (apply merge-with + collection))

(defn get-pageviews-for-collection
  "Returns a sorted map of key-value pairs in the following format
  ```:<date in the yyyyMMdd format> <sum of pageviews for pages on a wiki>```

  * `pages` should be a simple vector of page titles, for example:
    ```[\"Help:Toolforge\" \"Portal:Cloud VPS\"]```
  * `wiki` should match one of the wikis in [[config/permitted-wikis]]"
  [pages wiki]
  (->> pages
       (reduce (fn [l v] (conj l (get-pageviews-for-page v wiki))) [(dates-structure)])
       (calculate-sum)
       (into (sorted-map))))
