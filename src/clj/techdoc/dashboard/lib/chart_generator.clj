(ns techdoc.dashboard.lib.chart-generator
  "Functions for generating and converting charts
  displayed on the dashboard page."
  (:require [dali.io :as io]
            [dali.layout.place]
            [dali.layout.stack]))

(defn generate-bar-chart
  "Generates a bar chart based on values provided in a vector. For each
  record in the values vector, it displays a single bar on the chart.
  Expects a vector with exactly 90 values (one for every day) and will
  look weird with a vector of a different size.

  Example:
  ```(generate-bar-chart values \"90 values\" \"Value\" \"Date\")```

  You can generate an appropriate vector using: ```(into [] (range 1 91))```"
  [values title y-label x-label]
  (let [max-value (apply max values)
        bar-width 5     ;width of the record bar
        bar-gap 2       ;gap between bars
        m-l 50          ;margin-left
        m-r 11          ;margin-right
        m-t 44          ;margin-top
        m-b 22          ;margin-bottom
        inner-width (* 90 (+ bar-width bar-gap)) ;width of chart content
        outer-width (+ inner-width m-l m-r)      ;width of the entire chart
        inner-height 240                         ;height of chart content
        outer-height (+ inner-height m-t m-b)    ;height of the entire chart
        h-mid (quot inner-height 2)              ;half of the height of chart content
        h-quarter (quot inner-height 4)]         ;quarter of the height of chart content

    [:dali/page
     [:rect {:id :outer :stroke :none, :fill :white } [0 0] [outer-width outer-height]]
     [:rect {:id :bg :fill "rgb(235,235,235)" } [m-l m-t] [inner-width inner-height]]
     [:dali/place
      {:relative-to [:outer :top] :anchor :center :offset [0 (quot m-t 2)] :z-index 10}
      [:text.font-bold {:font-size 18} title]]
     [:dali/place
      {:relative-to [:bg :left] :anchor :left :offset [-40 0] :z-index 10}
      [:text.font-bold {:font-size 16 :transform [:rotate [-90 0 0]]} y-label]]
     [:dali/place
      {:relative-to [:bg :bottom] :anchor :center :offset [0 13] :z-index 10}
      [:text.font-bold {:font-size 16} x-label]]
     [:dali/stack
      {:position [m-l m-t], :direction :right, :anchor :bottom-left, :gap bar-gap}
      (map (fn [h]
             [:rect {:stroke :none, :fill :darkorchid} :_ [bar-width (quot (* h inner-height) max-value)]])
           values)]
     [:line {:id :upper :stroke :white } [m-l (+ m-t h-quarter)] [(- outer-width m-r) (+ m-t h-quarter)]]
     [:line {:id :mid :stroke :white } [m-l (+ m-t h-mid)] [(- outer-width m-r) (+ m-t h-mid)]]
     [:dali/place
      {:relative-to [:mid :left] :anchor :left :offset [-14 0] :z-index 10}
      [:text {:font-size 13 :transform [:rotate [-90 0 0]]} (str (quot max-value 2))]]
     [:line {:id :lower :stroke :white } [m-l (+ m-t h-mid h-quarter)] [(- outer-width m-r) (+ m-t h-mid h-quarter)]]]))

(defn chart-to-svg
  "Converts the chart generated using [[generate-bar-chart]] to SVG
  that you can embed in Hiccup."
  [chart]
  (io/render-svg-string chart))
