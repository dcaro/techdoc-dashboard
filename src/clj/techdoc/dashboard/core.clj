(ns techdoc.dashboard.core
  (:gen-class)
  (:require
    [clojure.tools.logging :as log]
    [integrant.core :as ig]
    [kit.edge.server.undertow]
    [kit.edge.utils.repl]
    [techdoc.dashboard.config :as config]
    [techdoc.dashboard.env :refer [defaults]]
    [techdoc.dashboard.web.handler]
    [techdoc.dashboard.web.routes.api]
    [techdoc.dashboard.web.routes.ui]))


;; log uncaught exceptions in threads
(Thread/setDefaultUncaughtExceptionHandler
  (reify Thread$UncaughtExceptionHandler
    (uncaughtException
      [_ thread ex]
      (log/error {:what :uncaught-exception
                  :exception ex
                  :where (str "Uncaught exception on" (.getName thread))}))))


(defonce system (atom nil))


(defn stop-app
  []
  ((or (:stop defaults) (fn [])))
  (some-> (deref system) (ig/halt!))
  (shutdown-agents))


(defn start-app
  [& [params]]
  ((or (:start params) (:start defaults) (fn [])))
  (->> (config/system-config (or (:opts params) (:opts defaults) {}))
       (ig/prep)
       (ig/init)
       (reset! system))
  (.addShutdownHook (Runtime/getRuntime) (Thread. stop-app)))


(defn -main
  [& _]
  (start-app))
