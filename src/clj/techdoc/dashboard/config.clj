(ns techdoc.dashboard.config
  (:require
    [datascript.core :as ds]
    [kit.config :as config]))


(def ^:const system-filename "system.edn")


(def ^{:doc "Currently supported languages.
       `:src` specifies the location of the translations file,
       `:dir` specifies text direction"}
  supported-languages
  {:en {:src "locale/en.json" :dir "ltr"}
   :pl {:src "locale/pl.json" :dir "ltr"}})


(def ^{:doc "List of wikis that can be analyzed using this instance of the dashboard."}
  permitted-wikis
  {"wikitechwikitechwikimedia" {:id "wikitech.wikimedia.org"
                                :article "https://wikitech.wikimedia.org/wiki/"
                                :api "https://wikitech.wikimedia.org/w/api.php"
                                :rest "https://wikitech.wikimedia.org/w/rest/"}
   "mediawikiwiki" {:id "mediawiki.org"
                    :article "https://mediawiki.org/wiki/"
                    :api "https://mediawiki.org/w/api.php"
                    :rest "https://mediawiki.org/w/rest/"}
   "metawiki" {:id "meta.wikimedia.org"
               :article "https://meta.wikimedia.org/wiki/"
               :api "https://meta.wikimedia.org/w/api.php"
               :rest "https://meta.wikimedia.org/w/rest/"}
   "commonswiki" {:id "commons.wikimedia.org"
                  :article "https://commons.wikimedia.org/wiki/"
                  :api "https://commons.wikimedia.org/w/api.php"
                  :rest "https://commons.wikimedia.org/w/rest/"}}) ; commons only added for test purposes as it's popular on PagePile

(def ^{:doc "DataScript connection initialization (single connection for the entire app). DataScript data is wiped on service restart."}
  ds-conn
  (ds/create-conn {:aka {:db/cardinality :db.cardinality/many}}))

(def
  ^{:doc "User agent added to the header of all outgoing API requests.
          Add your contact information to ensure your requests are not blocked."}
  user-agent
  "Techdoc.Dashboard")


(def ^{:doc "Datahike configuration"}
  dh-cfg
  {:store {:backend :mem :id "techdoc-dashboard"}
   :name "techdoc-dashboard"
   :schema-flexibility :read
   :keep-history? false})


(defn system-config
  [options]
  (config/read-config system-filename options))
