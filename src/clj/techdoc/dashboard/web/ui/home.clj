(ns techdoc.dashboard.web.ui.home
  (:require
    [techdoc.dashboard.web.controllers.locale :refer [translate tr] :as l]
    [techdoc.dashboard.web.controllers.pagepile :as pagepile]
    [techdoc.dashboard.web.htmx :refer [ui page] :as htmx]))


(defn button
  "Displays a form submission button"
  [{:keys [label disabled type]
    :or {label (translate :en :button-submit),
         disabled false,
         type "submit",}}]
  [:button {:class "btn" :type type :disabled disabled} label])


(defn home-page-ui
  "Framework for the home page. Used as a starting point for JS and non-JS version of the page"
  [l inner-content]
  [:div.card.items-center
   [:p.py-4.h-16 (translate l :specify-pagepile)]
   [:form.mt-12.md:mt-0.max-w-max.min-w-full.h-24.flex.flex-col.items-center.justify-center.md:flex-row
    {:action (str "/load-nojs?lang=" (name l)) :method "POST" :hx-post (str "/load?lang=" (name l)) :hx-target "#pile-content" :hx-swap "outerHTML" :hx-indicator "#spinner"}
    [:label.mt-4.md:mt-0.md:me-8 {:for "pagepile"} (str (translate l :pagepile-id) ":")]
    [:input.h-12.border-2.border-slate-600.shadow-inner-sharp.rounded.focus:drop-shadow-md.mt-4.md:mt-0.md:me-8 {:type "number" :id "pagepile" :name "pagepile" :required true}]
    (button {:label (translate l :button-load)})]
   [:div.mt-12.md:mt-4.py-4.min-w-full.max-w-max inner-content]])


(defn home-page
  "Starting page displayed regardless of whether JS is enabled or not"
  [{{:keys [lang]
     :or {lang "en"}} :params}]
  (page
    (keyword lang)
    (home-page-ui (keyword lang) [:div#pile-content.w-screen.px-3.md:w-auto {}])))


(defn home-page-no-js
  "Starting page with loaded PagePile contents. Only displayed when JS is not available in the browser"
  [{{:keys [pagepile lang]
     :or {lang "en"}} :params}]
  (page (keyword lang)
        (home-page-ui (keyword lang) [:div#pile-content.w-screen.px-3.md:w-auto
                                      [:h2 (tr lang :pagepile-contents)]
                                      [:p (tr lang :download-statistics-q) " "
                                       [:a.link {:href (str "/dashboard/" pagepile)} (tr lang :open-pagepile-stats)]]
                                      [:div
                                       [:div.text-ellipsis.overflow-x-auto.max-w-max {} (pagepile/get-pagepile pagepile (keyword lang))]]])))


(defn load-pile
  "Partial with PagePile contents. Loaded onto home page only when JS is available in the browser."
  [{{:keys [pagepile lang]
     :or {lang "en"}} :params}]
  (ui
    [:div#pile-content.w-screen.px-3.md:w-auto
     [:h2 (tr lang :pagepile-contents)]
     [:p (tr lang :download-statistics-q) " "
      [:a.link {:href (str "/dashboard/" pagepile)} (tr lang :open-pagepile-stats)]]
     [:span#spinner.htmx-indicator (tr lang :loading)]
     [:div.text-ellipsis.overflow-x-auto.max-w-max {} (pagepile/get-pagepile pagepile (keyword lang))]]))
