(ns techdoc.dashboard.web.ui.dashboard
  (:require
    [techdoc.dashboard.web.htmx :refer [page] :as htmx]
    [techdoc.dashboard.web.controllers.pagepile :as pagepile]
    [techdoc.dashboard.lib.mw-client :as mw]
    [clojure.tools.logging :as log]
    [techdoc.dashboard.lib.chart-generator :as ch]
    [techdoc.dashboard.web.controllers.locale :refer [tr]]))

(defn table-for-pageviews
  "Displays a table based on the pageviews map. Expects
  every k-v pair to be formatted as follows:

  * key - ```:YYYYMMDD00```
  * value - integer

  Generates a single table row for every k-v pair.

  Example:
  ```(table-for-pageviews {:2023052400 1} \"en\")```"
  [pageviews-for-collection lang]
  (let [tbody (reduce-kv #(conj %1 [:tr nil [:td.border nil (subs (name %2) 0 8)] [:td.border nil %3]])
                         [:tbody nil]
                         pageviews-for-collection)]
    [:table.table-fixed.border-collapse.border.border-slate-400.w-full nil [:thead nil
                                                                            [:tr [:th.border {:class "w-1/2"} (tr lang :dashboard-date)] [:th.border {:class "w-1/2"} (tr lang :dashboard-views)]]]
     tbody]))

(defn pageviews-widget
  "Displays the widget with page view data for the collection.
  Typically called from within [[dashboard]].

  Expects the following parameters:

  * title - displayed in the header
  * pageviews-for-collection - pageviews data returned by [[mw/get-pageviews-for-collection]]
  * display-chart - boolean value to indicate whether to display a chart
  * lang - language
  "
  [title pageviews-for-collection display-chart lang]
  (let [pageview-values-vector (into [] (vals pageviews-for-collection))]
    [:div.widget.overflow-auto nil
     [:div.mb-2.bg-slate-100.h-12.p-2 {} title]
     (when display-chart  [:div.overflow-x-auto nil [:div.chart-box nil (ch/chart-to-svg (ch/generate-bar-chart pageview-values-vector (tr lang :pageviews-chart-title) (tr lang :pageviews-chart-y) (tr lang :pageviews-chart-x)))]])
     [:div.px-2.pb-2.max-h-80.overflow-y-auto {} (table-for-pageviews pageviews-for-collection lang)]]))

(defn dashboard
  "Displays the dashboard page. Takes pagepile-id to display data for from path.
  Takes language from params (```?lang=x```) and defaults to \"en\".

  Depending on the size of the pile, might take a long time to run. Runs all
  calculations to display in every widget."
  [{{:keys [pagepile-id]} :path-params
    {:keys [lang]
     :or {lang "en"}} :params}]
  (let [p (pagepile/get-pagepile-structured pagepile-id (keyword lang))
        pageviews-for-collection (mw/get-pageviews-for-collection (:pages p)(:wiki p))]
    (log/info "Display dashboard for pagepile: " pagepile-id)
    (if (:error p)
      (page
       (keyword lang)
       [:div.card
        [:p (str (tr lang :dashboard-error) " ")
         [:a.link {:href "/"} (tr lang :dashboard-startover)]]])
      (page
        (keyword lang)
        [:div.card
         [:h1.mb-8.font-medium (str (tr lang :dashboard-title) " " pagepile-id)]
         [:div.grid.gap-2.grid-cols-1.md:grid-cols-2
          (pageviews-widget (tr lang :pageviews-widget-title)
                  pageviews-for-collection
                  true
                  lang)]
         [:div.m-2.mt-4
          [:pre {} "PagePile to load: " pagepile-id]]]))))
