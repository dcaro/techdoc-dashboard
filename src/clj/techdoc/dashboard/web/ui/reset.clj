(ns techdoc.dashboard.web.ui.reset
  (:require
    [techdoc.dashboard.web.controllers.cache :as cache]
    [techdoc.dashboard.web.htmx :refer [page] :as htmx]))


(defn reset
  [request]
  (cache/reset-dh)
  (page
    [:div "Cache reset"]))
