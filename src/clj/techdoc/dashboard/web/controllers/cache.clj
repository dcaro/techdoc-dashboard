(ns techdoc.dashboard.web.controllers.cache
  "Used to interact with cache. Includes experimental features for working with DataScript and datahike"
  (:require
    [clojure.core.cache.wrapped :as w]
    [datahike.api :as dh]
    [datascript.core :as d]
    [techdoc.dashboard.config :as c]))


(def ^{:doc "Atom that holds the cache. Records expire after 60 minutes."}
  C
  (w/ttl-cache-factory {} :ttl 3600000))


(when (not (dh/database-exists? c/dh-cfg)) (dh/create-database c/dh-cfg)) ; creates a datahike instance if it does not exist

(defn add-pagepile
  "[Experimental] Adds the ID of a pile to DataScript and datahike"
  [pagepile]
  (let [dh-conn (dh/connect c/dh-cfg)]
    (d/transact c/ds-conn [{:pagepile-id pagepile}])
    (dh/transact dh-conn [{:pagepile-id pagepile}])
    (dh/release dh-conn)))


(defn fetch-all-records-ds
  "[Experimental] Returns all records stored in DataScript"
  []
  (d/q '[:find ?e ?a ?v
         :where [?e ?a ?v]]
       @c/ds-conn))


(defn fetch-all-records-dh
  "[Experimental] Returns all records stored in datahike"
  []
  (let [dh-conn (dh/connect c/dh-cfg)
        result (dh/q '[:find ?e ?a ?v
                       :where [?e ?a ?v]]
                     @dh-conn)]
    (dh/release dh-conn)
    result))


(defn fetch-cache-record
  "Wrapper around lookup-or-miss. For key `k` and function `f`, executes function `f` if the record with key `k` is not found in cache"
  [k f]
  (w/lookup-or-miss C k f))


(defn just-fetch-record
  "For key `k`, returns a record in cache with that key, or nil if it's not found"
  [k]
  (w/lookup C k))


(defn reset-dh
  "[Experimental] Resets datahike database"
  []
  (dh/delete-database c/dh-cfg)
  (dh/create-database c/dh-cfg))
