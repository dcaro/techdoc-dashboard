(ns techdoc.dashboard.web.controllers.locale
  (:require
    [cheshire.core :refer [parse-string]]
    [clojure.java.io :as io]
    [techdoc.dashboard.config :refer [supported-languages]]
    [tongue.core :as tongue]))


(defn reformat-translations-json
  "Reformats translations data loaded from the JSON file to use it with the Tongue library"
  [tr]
  (->> (dissoc tr "@metadata")
       (seq)
       (map (fn [r] [(keyword (first r)) (last r)]))
       (into {})))


(defn load-translation-file
  "Loads and reformats translations data from JSON files.
  These files will be loaded into the JAR during build and don't need to be present in production"
  [f]
  (let [file-content (slurp (io/resource f))]
    (-> file-content
        parse-string
        reformat-translations-json)))


(defn get-language-dir
  "Returns text direction for language `l` specified in the configuration"
  [l]
  (or (get-in supported-languages [l :dir]) "ltr"))


(def
  ^{:doc "Generates the translations dictionary based on `supported-languages` specified in configuration"}
  dict
  (reduce-kv (fn [m k v] (assoc m k (load-translation-file (:src v)))) {:tongue/fallback :en} supported-languages))


(def
  ^{:doc "Builds the translate function. To use it, run `(translate <lang> <translation key>)`"}
  translate
  (tongue/build-translate dict))


(defn tr
  "Returns translation based on `l` defined as a string (keyword generated automatically)
  and `k`"
  [l k]
  (translate (keyword l) k))
