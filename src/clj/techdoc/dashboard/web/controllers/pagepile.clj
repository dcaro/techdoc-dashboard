(ns techdoc.dashboard.web.controllers.pagepile
  "Used to interact with the PagePile API"
  (:require
    [cheshire.core :refer [parse-string]]
    [clj-http.client :as client]
    [clojure.string :as str]
    [hiccup.core]
    [techdoc.dashboard.config :refer [permitted-wikis user-agent]]
    [techdoc.dashboard.web.controllers.cache :refer [fetch-cache-record]]
    [techdoc.dashboard.web.controllers.locale :refer [translate]]
    [clojure.tools.logging :as log]))


(defn load-pagepile
  "Takes an `ID` and queries the pagepile API to fetch data for the pile indicated by the ID"
  [id]
  (try (client/get (format "https://pagepile.toolforge.org/api.php?id=%s&action=get_data&doit&format=json&metadata=1"
                           id)
                   {:headers {"User-Agent" user-agent}}
                   {:as :json})
       (catch Exception e
         (let [status (:status (.getData e))
               reason (:reason-phrase (.getData e))]
           {:status status :body reason}))))

(defn load-pagepile-basedon-keyword
  "Takes a `key` that includes the PagePile ID as its last part (name), and runs load-pagepile using that ID"
  [key]
  (load-pagepile (name key)))


(defn load-pagepile-through-cache
  "Takes an `id` and loads data related to the PagePile with this ID from cache. If it's not found,
  it runs load-pagepile-basedon-keyword using a keyword constructed from this ID and stores its returned
  data in cache"
  [id]
  (fetch-cache-record (keyword "pagepile" (str id)) #(load-pagepile-basedon-keyword %)))


(defn validate-pagepile
  "Validates PagePile API server response provided by `load-pagepile`
  and returns a data structure with pages in the pile (if the response is correct),
  or the error message (if the response indicates any errors)"
  [{:keys [body status]} l]
  (cond
    (not (= status 200)) {:status :error :body (translate l :pagepile-cannot-connect) :original-body body}
    (or (str/includes? body "Uncaught Exception")
        (str/includes? body "Uncaught Error")) {:status :error :body (translate l :pagepile-invalid-id)}
    (not (contains? permitted-wikis (get-in (parse-string body) ["wiki"]))) {:status :error :body (translate l :pagepile-unsupported-wiki)}
    :else {:status :ok :body (get (parse-string body) "pages") :wiki (get (parse-string body) "wiki")}))


(defmulti print-pagepile-response
  "Formats and prints the PagePile API response validated in `validate-pagepile`.
  The response contains a list of pages in the given pile in case of success,
  and an error message if an error occurred."
  :status)


(defmethod print-pagepile-response :ok
  ;; No errors
  [{:keys [body wiki]}]
  (let [wiki-url (get-in permitted-wikis [wiki :article])]
    [:ul.leading-8 (->> body
                        (keys)
                        (map (fn [p]
                               [:li.list-disc.list-inside {}
                                [:a.link.underline.decoration-solid.decoration-auto {:href (hiccup.core/h (str wiki-url p))} (hiccup.core/h p)]])))]))

(defmethod print-pagepile-response :error
  ;; Errors
  [{:keys [body]}]
  [:div {} [:p body]])


(defn get-pagepile-structured
  "Fetches and validates the response of a request to PagePile API.
  The request is for the contents of a pile indicated by `ID`.

  Unlike [[get-pagepile]], returns a simple data structure that must
  be processed before displaying.
  "
  [id l]
  (log/info "Getting pagepile: " id)
  (let [pagepile-response (-> id
                              (load-pagepile-through-cache)
                              (validate-pagepile l))]
    (cond
      (= id -1) {:pages ["Help:Toolforge" "Help:Toolforge/Pywikibot"] :wiki "wikitechwikitechwikimedia" :error false}
      (= (:status pagepile-response) :ok) {:pages (into [] (keys (:body pagepile-response))) :wiki (:wiki pagepile-response) :error false}
      :else {:pages nil :wiki nil :error true})))

(defn get-pagepile
  "Fetches, validates, and formats the response of the request to the PagePile API.
  The request is for the contents of a pile indicated by `ID`"
  [id l]
  (-> id
      (load-pagepile-through-cache)
      (validate-pagepile l)
      (print-pagepile-response)))
