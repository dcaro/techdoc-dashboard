(ns techdoc.dashboard.web.htmx
  (:require
    [hiccup.core :as h]
    [hiccup.page :as p]
    [ring.util.http-response :as http-response]
    [techdoc.dashboard.config :refer [supported-languages]]
    [techdoc.dashboard.web.controllers.locale :refer [translate get-language-dir]]))


(defn base
  "Base template for all UI content displayed in the browser"
  [l content]
  [:head
   [:meta {:charset "UTF-8"}]
   [:title "Technical documentation dashboard"]
   (p/include-js "https://tools-static.wmflabs.org/cdnjs/ajax/libs/htmx/1.9.1/htmx.min.js")
   (p/include-js "/js/hyperscript.min.js")
   (p/include-css "/css/app.css")
   [:body.bg-no-repeat.bg-fixed.bg-gradient-to-br.from-slate-400.to-slate-500.text-xl
    [:div.flex.flex-col.h-screen
     [:nav.flex.flex-col.md:flex-row.flex-wrap.md:flex-nowrap.md:justify-between.items-center.bg-slate-50.w-full.h-12
      [:a.md:ms-4 {:href (str "/?lang=" (name l))} (translate l :app-name)]
      [:nav.flex.flex-col.md:flex-row.md:justify-between.md:me-4
       [:a.md:me-4 {:href "?lang=en"} "en"]
       [:a.md:me-4 {:href "?lang=pl"} "pl"]]]
     [:div.mb-auto content]
     [:footer.bg-slate-50.h-12.md:text-sm.px-4.text-center
      [:p (translate l :footer-text
                     (h/html [:a.link {:href "https://creativecommons.org/publicdomain/zero/1.0/"} "CC0"])
                     (h/html [:a.link {:href "#"} (translate l :license)])
                     (h/html [:a.link {:href "#"} (translate l :repo)]))]]]]])


(defn page
  "Generates page HTML using Hiccup. Use when rendering the entire page."
  [l content]
  (-> (p/html5 {:dir (get-language-dir l)} (base l content))
      http-response/ok
      (http-response/content-type "text/html")))


(defn ui
  "Generates page element HTML using Hiccup. Use when rendering parts of a page."
  [content]
  (-> (h/html content)
      http-response/ok
      (http-response/content-type "text/html")))
