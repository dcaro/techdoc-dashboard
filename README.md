# Techdoc Dashboard

**Status**: work in progress (pre-alpha)

The application isn't ready for production use. Many features are missing and there is a lot of experimental code and unnecessary dependencies. Clone and run at your own risk.

This application uses the [Kit framework](https://kit-clj.github.io/). To learn more about its fundamental architecture and design, see the framework's documentation.

## Requirements

Install [Clojure](https://clojure.org/guides/install_clojure).

You can optionally install [babashka](https://babashka.org/). This gives you access to the tasks defined in ``bb.edn``. These tasks are a faster way of running the important Clojure commands.

If you plan to build the project or make changes to CSS classes, check that you can run ``npx tailwindcss --help``. This requires NodeJS and NPM. Note that future versions will remove this dependency.

## Repository branching model

This repository uses the [git-flow](https://nvie.com/posts/a-successful-git-branching-model/) branching model. The following branches are available:

* `main` - production release branch.
* `develop` - development and integration branch. All new features are merged into this branch first, before being merged to main for production releases. Some development can also happen directly on this branch.
* `feature/*` - feature branches. Used for new functionality.

## Running in development

Before running the application, be sure to include your SUL account name or other identifying information in the `user-agent` variable in `config.clj`. Otherwise your API requests might get blocked.

Start a [REPL](#repl) in your editor or terminal of choice. If you don't know what a REPL is, see this [introduction to REPL](https://clojure.org/guides/repl/introduction).

Start the application with:

```clojure
(go)
```

The application's homepage is available at http://localhost:3000/.

System configuration is available under `resources/system.edn`.

To reload changes:

```clojure
(reset)
```

If you want to rebuild the CSS bundle every time you make relevant changes to the code, run ``npx tailwindcss -i ./resources/tailwind.css -o ./resources/public/css/app.css --watch`` from project's root.

## Building code documentation

Run ``bb codox`` to generate a static HTML representation of code documentation strings. All documentation files are available in ``target/doc``.

## REPL

### Visual Studio Code/VSCodium

Install [Calva](https://calva.io/). Start the REPL in the terminal by running `bb cider` or `clj -M:dev:cider` in the root directory. Then run the Calva command to connect to a running, non-production REPL inside Visual Studio Code. You can also use the jack-in command directly from the editor (without starting the server first), in which case be sure to select `techdoc-dashboard` (or the name of your project directory), then `deps.edn`, and then the `:cider` and `:dev` aliases.

### Cursive

Configure a [REPL following the Cursive documentation](https://cursive-ide.com/userguide/repl.html). Using the default "Run with IntelliJ project classpath" option will let you select an alias from the "Clojure deps" aliases selection.

### Emacs with CIDER

Use the `cider` alias for CIDER nREPL support (run `bb cider` or `clj -M:dev:cider`). See the [CIDER docs](https://docs.cider.mx/cider/basics/up_and_running.html) for more help.

This alias runs nREPL with CIDER during development.

### Command line

Run `bb nrepl`, `clj -M:dev:nrepl`, or `make repl`.

Note that, just like with [CIDER](#cider), this alias runs nREPL during development.

## Managing dependencies

Use [antq](https://github.com/liquidz/antq). Make sure you are running Clojure 1.11.1.1139 or higher and install antq globally based on the [instructions available on GitHub](https://github.com/liquidz/antq#clojure-cli-tools-11111139-or-later). Then run one of the following commands:

```bash
# To display outdated dependencies:
clojure -Tantq outdated

# To upgrade outdated dependencies:
clojure -Tantq outdated :upgrade true
# also available as a bb task
bb upgrade

# To upgrade outdated dependencies, including your globally installed tools:
clojure -Tantq outdated :check-clojure-tools true :upgrade true
```

## Adding translation languages

To add a new translation language, create a new file in `resources/locale`. Use the existing translation files as a starting point. Then add the language to `supported-languages` in `src/clj/techdoc/dashboard/config.clj`. Be sure to specify the path to the language file.

## License

Unless noted otherwise in the heading of a given file or on the list below, all code in this repository is licensed under the terms of Eclipse Public License 2.0 (full text available in the LICENSE file). Licenses and copyright notices for dependencies are available in the DEPENDENCIES.txt file.

* `resources/public/css/app.css` - CSS file generated using tailwindcss, licensed under the terms of the MIT license as described in DEPENDENCIES.txt > tailwindcss.
* `resources/public/js/hypesrcipt.min.js` - Minified code of hyperscript, licensed under the terms of the BSD 2-Clause license as described in DEPENDENCIES.txt > hyperscript.
